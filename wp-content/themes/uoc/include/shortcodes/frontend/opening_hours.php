<?php

/*
 *
 * @File : List
 * @retrun
 *
 */
if (!function_exists('cs_list_openinghours')) {

    function cs_list_openinghours($atts, $content = "") {
        global $cs_border, $cs_list_type;
        $defaults = array(
            'column_size' => '',
            'cs_list_section_title' => '',
            'cs_list_type' => '',
            'cs_list_icon' => '',
            'cs_border' => '',
            'cs_list_item' => '',
            'cs_list_class' => '',
            'cs_schadule_text' => ''
        );
        extract(shortcode_atts($defaults, $atts));
        $customID = '';
        if (isset($column_size) && $column_size != '') {
            $column_class = cs_custom_column_class($column_size);
        } else {
            $column_class = '';
        }
        if (isset($cs_list_class) && $cs_list_class != '') {
            $customID = 'id="' . $cs_list_class . '"';
        }
        $html = "";
        $cs_list_typeClass = '';
        $section_title = '';
        if ($cs_list_section_title && trim($cs_list_section_title) != '') {

            $section_title = '<div class="cs-section-title"><h4>' . esc_html($cs_list_section_title) . '</h4></div>';
        }

        $html .= '
		   <div class="opening-hours ' . $column_class . '">';
        $html .=$section_title;
        $html .='<div class="cs-opening">';
        $html .= '<ul>';
        // $html .= do_shortcode($content);
        $query = new WP_Query( array( 
            'post_type' => 'events',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_key'          => 'cs_event_start_time',
            'orderby'           => 'meta_value',
            'order'             => 'ASC',
            'meta_type'         => 'DATE',
            'tax_query' => [
                [
                    'taxonomy' => 'event-category',
                    'field' => 'slug',
                    'terms' => 'charlas-informativas',
                ]
            ]
        ) );
        $posts = $query->get_posts();
        foreach( $posts as $row ) {
            $fecha = get_post_meta($row->ID, 'cs_event_from_date');
            // $explode = explode('/', $fecha[0]);
            // $fecha = \DateTime::createFromFormat("m/d/Y", $fecha[0]);
            // ->format("D d \d\e m \d\e Y")
            $hora = get_post_meta($row->ID, 'cs_event_start_time') ? get_post_meta($row->ID, 'cs_event_start_time')[0] : '';
            setlocale(LC_TIME, 'es_ES');
            $html .= '<li><span class="day">'.ucfirst(date_i18n('l j \d\e F \d\e Y', strtotime($fecha[0]))).'</span>
                <div class="timehoure">
                    <span class="time-start"><i class="icon-clock"></i> '.$hora.'</span>
                </div></li>';
        }
        $html .= '</ul>';
        $html .= '</div>
			</div>';
        return $html;
    }

    if (function_exists('cs_short_code'))
        cs_short_code(CS_SC_OPENINGHOURS, 'cs_list_openinghours');
}

if (!function_exists('cs_openinghours_item_shortcode')) {

    function cs_openinghours_item_shortcode($atts, $content = "") {
        global $cs_border, $cs_list_type;
        $html = '';
        $defaults = array('cs_list_icon' =>
            '', 'cs_list_item' =>
            '', 'cs_cusotm_class' =>
            '', 'cs_custom_animation' =>
            '', 'cs_custom_animation' =>
            '',
            'cs_schadule_text' => ''
        );
        extract(shortcode_atts($defaults, $atts));
        if ($cs_border == 'yes') {
            $border = 'border-bottom:1px solid #e4e4e4;';
        } else {
            $border = '';
        }

        $html .= '<li>
				<span class="day">' . $cs_list_item . '</span>
				<div class="timehoure">
					<span class="time-start"><i class="icon-clock"></i>' . do_shortcode(htmlspecialchars_decode($content)) . '</span>
				</div>
			</li>';

        return $html;
    }

    if (function_exists('cs_short_code'))
        cs_short_code(CS_SC_OPENINGHOURS_LISTITEM, 'cs_openinghours_item_shortcode');
}