<?php
global $cs_page_option;
cs_include_file(ABSPATH . '/wp-admin/includes/file.php');
// Demo 1
$home_demo = cs_get_demo_content('home.json');
$home2_demo = cs_get_demo_content('home2.json');
$home3_demo = cs_get_demo_content('home3.json');
$home4_demo = cs_get_demo_content('home4.json');
$home_rtl_demo = cs_get_demo_content('home_rtl.json');
$cs_page_option[] = array();
$cs_page_option['theme_options'] = array(
    'select' => array(
        'home' => 'Demo V1',
        'home2' => 'Demo V2',
        'home3' => 'Demo V3',
        'home4' => 'Demo V4',
        'home_rtl' => 'Demo RTL',
        
    ),
    'home' => array(
        'name' => 'Demo V1',
        'page_slug' => 'home',
        'theme_option' => $home_demo,
        'thumb' => 'home'
    ),
    'home2' => array(
        'name' => 'Demo V2',
        'page_slug' => 'home-v2',
        'theme_option' => $home2_demo,
        'thumb' => 'home-v2'
    ),
    'home3' => array(
        'name' => 'Demo V3',
        'page_slug' => 'home-v3',
        'theme_option' => $home3_demo,
        'thumb' => 'home-v3'
    ),
    'home4' => array(
        'name' => 'Demo V4',
        'page_slug' => 'home-v4',
        'theme_option' => $home4_demo,
        'thumb' => 'home-v4'
    ),
    'home_rtl' => array(
        'name' => 'Demo RTL',
        'page_slug' => 'home-rtl',
        'theme_option' => $home_rtl_demo,
        'thumb' => 'home-rtl'
    ),
);