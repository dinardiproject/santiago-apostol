<?php
global $post, $cs_blog_cat, $cs_blog_description, $cs_blog_excerpt, $cs_title_excerpt, $cs_notification, $wp_query, $px_blog_cat;
extract($wp_query->query_vars);

$width = '150';
$height = '150';
$title_limit = 1000;
?> 
<div class="cs-campunews custom-fig col-md-12">
    <?php
        $categoria = get_category_by_slug($args['category_name']);
        $color = $categoria->category_description ? $categoria->category_description : '#000000';
    ?>
    <label style="background: <?php echo $color; ?>"><span class="triangulo" style="border-color: <?php echo $color; ?> transparent transparent transparent;"></span><?php echo $categoria->name; ?></label>
    <ul>
        <?php
        $query = new WP_Query($args);
        $post_count = $query->post_count;
        if ($query->have_posts()) {
            $postCounter = 0;
            while ($query->have_posts()) : $query->the_post();
                $thumbnail = cs_get_post_img_src($post->ID, $width, $height);
                $cs_postObject = get_post_meta($post->ID, "cs_full_data", true);
                $cs_gallery = get_post_meta($post->ID, 'cs_post_list_gallery', true);
                $cs_gallery = explode(',', $cs_gallery);
                $cs_thumb_view = get_post_meta($post->ID, 'cs_thumb_view', true);
                $cs_post_view = isset($cs_thumb_view) ? $cs_thumb_view : '';
                $current_user = wp_get_current_user();
                $post_tags_show = get_post_meta($post->ID, 'cs_post_tags_show', true);
                $custom_image_url = get_user_meta(get_the_author_meta('ID'), 'user_avatar_display', true);
                $cs_post_like_counter = get_post_meta(get_the_id(), "cs_post_like_counter", true);
                if (!isset($cs_post_like_counter) or empty($cs_post_like_counter))
                    $cs_post_like_counter = 0;
                $tags = get_tags();
                ?>
                <li style="border-color: <?php echo $color; ?>!important;">
                    <?php if ($thumbnail <> '' && 0) {
                        ?>
                        <figure>
                            <a href="<?php esc_url(the_permalink()); ?>"><img src="<?php echo esc_url($thumbnail); ?>" alt=""></a>


                            <figcaption>
                                <a href="<?php echo esc_url($thumbnail); ?>" rel="prettyPhoto[gallery2]" title="">

                                </a>
                            </figcaption>
                        </figure>
                    <?php } ?>
                    <div class="cs-campus-info noticia">
                        <!-- <div class="cs-newscategorie">  <?php //cs_get_categories($px_blog_cat); ?> </div> -->
                        <p class="fecha"><?php echo date_i18n('l j \d\e F', strtotime(get_the_date('Y-m-d'))); ?></p>
                        <h6><a href="<?php esc_url(the_permalink()); ?>" style="color: <?php echo $color; ?>!important;"><?php
                                if (isset($cs_title_excerpt) && !empty($cs_title_excerpt)) {

                                    echo wp_trim_words(get_the_title(), $cs_title_excerpt);
                                } else {


                                    the_title();
                                }
                                ?></a></h6>
                                <?php the_excerpt(); ?>
                    </div>
                </li>

                <?php
            endwhile;
        } else {
            $cs_notification->error('No blog post found.');
        }
        ?>
    </ul>


</div>








