=== FG Drupal to WordPress Premium Internationalization module ===
Contributors: Frédéric GILLES
Plugin Uri: https://www.fredericgilles.net/fg-drupal-to-wordpress/internationalization/
Tags: drupal, wordpress, importer, migrator, converter, import, internationalization, wpml, multilang
Requires at least: 4.5
Tested up to: 5.1.1
Stable tag: 1.0.3
Requires PHP: 5.3
License: GPLv2

A plugin to migrate the Drupal translations to WPML (WordPress)
Needs «FG Drupal to WordPress Premium» and WPML plugins to work

== Description ==

This is the Internationalization module. It works only if the plugin FG Drupal to WordPress Premium and the WPML plugin are already installed.
It has been tested with **Drupal 6, 7 and 8**, **WPML 4.2** and **Wordpress 5.1**. It is compatible with multisite installations.

Major features include:

* migrates the nodes translations
* migrates the taxonomies translations

== Installation ==

1.  Prerequesite: Buy and install the plugin «FG Drupal to WordPress Premium»
2.  Extract plugin zip file and load up to your wp-content/plugin directory
3.  Activate Plugin in the Admin => Plugins Menu
4.  Run the importer in Tools > Import > Drupal

== Translations ==
* English (default)
* French (fr_FR)
* other can be translated

== Changelog ==

= 1.0.3 =
Fixed: Categories translations whose name is the same as in the original language were not imported
Tested with WordPress 5.1
Tested with WPML 4.2

= 1.0.2 =
Fixed: Some categories imported in wrong language
Tested with WordPress 4.8
Tested with WPML 3.8

= 1.0.1 =
Fixed: The language codes with more than 2 characters (eg. pt-br) were not imported

= 1.0.0 =
Initial version
