<?php
/*
Plugin Name:       Podamibe Facebook Widget
Plugin URI:        http://podamibenepal.com/wordpress-plugins/
Description:       This plugin is used for displaying facebook feeds on your desired sidebars with various settings.
Version:           1.0.4
Author:            Podamibe Nepal
Author URI:        http://podamibenepal.com/ 
License:           GPLv2 or later
Text Domain:       pfw
*/

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'PFW_PATH', plugin_dir_path( __FILE__ ) );
include( PFW_PATH . 'inc/podamibe-facebook-widget.php');

/**
* Register and enqueue the required script
*/

function pfw_register_widget_scripts() {
	wp_enqueue_style( 'pfw-main-style', plugin_dir_url( __FILE__ ) . 'assets/pfw-style.css');
	wp_register_style( 'pfw-font-awesome', plugin_dir_url( __FILE__ ) . 'assets/font-awesome.min.css');
	wp_enqueue_style( 'pfw-font-awesome' );
}
add_action( 'wp_enqueue_scripts', 'pfw_register_widget_scripts' );