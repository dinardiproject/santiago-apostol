#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: CS Framework\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-05 05:28+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: cs-framework.php:107
msgid "UOCE"
msgstr ""

#: cs-framework.php:113
msgid "Theme Support"
msgstr ""

#: cs-framework.php:114
msgid "Customization"
msgstr ""

#: cs-framework.php:115
msgid "Documentation"
msgstr ""

#: include/cs-importer/class-widget-data.php:361
msgid "Error saving file!"
msgstr ""

#: include/cs-importer/class-widget-data.php:363
msgid "Backup Generated."
msgstr ""

#: include/cs-importer/class-widget-data.php:392
#, php-format
msgid "File '%s' Deleted Successfully"
msgstr ""

#: include/cs-importer/class-widget-data.php:394
msgid "Error Deleting file!"
msgstr ""

#: include/cs-importer/class-widget-data.php:486
msgid "Widgets Imported Successfully."
msgstr ""

#: include/cs-importer/theme_importer.php:6
#: include/cs-importer/theme_importer.php:156
msgid "Import Demo Data"
msgstr ""

#: include/cs-importer/theme_importer.php:53
msgid "Error."
msgstr ""

#: include/cs-importer/theme_importer.php:57
msgid "Sorry, there has been an error."
msgstr ""

#: include/cs-importer/theme_importer.php:58
msgid "The file does not exist, please try again."
msgstr ""

#: include/cs-importer/theme_importer.php:148
msgid "You have already install this demo."
msgstr ""

#: include/cs-importer/theme_importer.php:159
msgid ""
"Importing demo data helps to build site like the demo site by all means. It "
"is the quickest way to setup theme. Following things happen when dummy data "
"is imported;"
msgstr ""

#: include/cs-importer/theme_importer.php:161
msgid "All wordpress settings will remain same and intact"
msgstr ""

#: include/cs-importer/theme_importer.php:162
msgid "Posts, pages and dummy images shown in demo will be imported"
msgstr ""

#: include/cs-importer/theme_importer.php:163
msgid ""
"Only dummy images will be imported as all demo images have copy right "
"restriction"
msgstr ""

#: include/cs-importer/theme_importer.php:164
msgid ""
"No existing posts, pages, categories, custom post types or any other data "
"will be deleted or modified"
msgstr ""

#: include/cs-importer/theme_importer.php:165
msgid "To proceed, please click \"Import Demo Data\" and wait for a while"
msgstr ""

#: include/cs-importer/theme_importer.php:166
msgid ""
"Note: Before Import demo data please make sure your server has following "
"Setting"
msgstr ""

#: include/cs-importer/theme_importer.php:169
msgid "post_max_size = 128M or Greater"
msgstr ""

#: include/cs-importer/theme_importer.php:170
msgid "upload_max_filesize = 128M or Greater"
msgstr ""

#: include/cs-importer/theme_importer.php:171
msgid "memory_limit = 256M or Greater"
msgstr ""

#: include/cs-importer/theme_importer.php:172
msgid "max_input_vars = 5000M or Greater"
msgstr ""

#: include/cs-importer/theme_importer.php:173
msgid "max_execution_time = 300 or Greater"
msgstr ""

#: include/cs-importer/theme_importer.php:176
msgid "Your Current Setting"
msgstr ""

#: include/cs-importer/theme_importer.php:259
msgid "Theme Settings Saved."
msgstr ""

#: include/cs-mailchimp/mailchimp_functions.php:61
msgid "Subscribe Weekly Newsletter"
msgstr ""

#: include/cs-mailchimp/mailchimp_functions.php:70
msgid "Enter Valid Email Address"
msgstr ""

#: include/cs-mailchimp/mailchimp_functions.php:71
msgid "Submit"
msgstr ""

#: include/meta-boxes/campus_meta.php:18
msgid "Campus Options"
msgstr ""

#: include/meta-boxes/campus_meta.php:37 include/meta-boxes/course_meta.php:41
#: include/meta-boxes/event_meta.php:38 include/meta-boxes/team_meta.php:40
msgid "Header Absolute"
msgstr ""

#: include/meta-boxes/campus_meta.php:41 include/meta-boxes/event_meta.php:46
msgid "Location"
msgstr ""

#: include/meta-boxes/campus_meta.php:70 include/meta-boxes/course_meta.php:121
#: include/meta-boxes/event_meta.php:120
msgid "Location Map"
msgstr ""

#: include/meta-boxes/campus_meta.php:78 templates/course/single-course.php:165
msgid "Campus Location"
msgstr ""

#: include/meta-boxes/campus_meta.php:83 include/meta-boxes/course_meta.php:134
#: include/meta-boxes/event_meta.php:133 include/meta-boxes/team_meta.php:130
msgid "Address"
msgstr ""

#: include/meta-boxes/campus_meta.php:93
msgid "latitude"
msgstr ""

#: include/meta-boxes/campus_meta.php:103
msgid "longitude"
msgstr ""

#: include/meta-boxes/campus_meta.php:112
#: include/meta-boxes/course_meta.php:143 include/meta-boxes/event_meta.php:142
msgid "City / Town"
msgstr ""

#: include/meta-boxes/campus_meta.php:121
#: include/meta-boxes/course_meta.php:152 include/meta-boxes/event_meta.php:151
msgid "Post Code"
msgstr ""

#: include/meta-boxes/campus_meta.php:130
#: include/meta-boxes/course_meta.php:161 include/meta-boxes/event_meta.php:160
msgid "Region"
msgstr ""

#: include/meta-boxes/campus_meta.php:146
#: include/meta-boxes/course_meta.php:176 include/meta-boxes/event_meta.php:174
msgid "Country"
msgstr ""

#: include/meta-boxes/campus_meta.php:159
#: include/meta-boxes/course_meta.php:189 include/meta-boxes/event_meta.php:187
msgid "Search This Location on Map"
msgstr ""

#: include/meta-boxes/course_meta.php:21 include/meta-boxes/course_meta.php:50
msgid "Course Options"
msgstr ""

#: include/meta-boxes/course_meta.php:36 include/meta-boxes/event_meta.php:32
msgid "General"
msgstr ""

#: include/meta-boxes/course_meta.php:44 include/meta-boxes/event_meta.php:41
#: include/meta-boxes/team_meta.php:43
msgid "Seo Options"
msgstr ""

#: include/meta-boxes/course_meta.php:94 include/meta-boxes/event_meta.php:93
msgid "Social Sharing"
msgstr ""

#: include/meta-boxes/course_meta.php:103 include/meta-boxes/event_meta.php:102
msgid "Tags"
msgstr ""

#: include/meta-boxes/course_meta.php:129 include/meta-boxes/event_meta.php:128
msgid "Event Location"
msgstr ""

#: include/meta-boxes/course_meta.php:283
msgid "Course Schedule"
msgstr ""

#: include/meta-boxes/course_meta.php:292
msgid "Course Start Date"
msgstr ""

#: include/meta-boxes/course_meta.php:301
msgid "Course End Date"
msgstr ""

#: include/meta-boxes/course_meta.php:311 include/meta-boxes/event_meta.php:305
#: include/meta-boxes/event_meta.php:329 include/meta-boxes/event_meta.php:347
#: include/meta-boxes/event_meta.php:367
msgid "Wrapper"
msgstr ""

#: include/meta-boxes/course_meta.php:317
msgid "Course Start Time"
msgstr ""

#: include/meta-boxes/course_meta.php:327
msgid "Course End Time"
msgstr ""

#: include/meta-boxes/course_meta.php:337
msgid "Course Descriptions"
msgstr ""

#: include/meta-boxes/course_meta.php:346
msgid "Course Duration"
msgstr ""

#: include/meta-boxes/course_meta.php:355
msgid "Course Code"
msgstr ""

#: include/meta-boxes/course_meta.php:366
msgid "Degree Level"
msgstr ""

#: include/meta-boxes/course_meta.php:376
msgid "Degree Level Background Color"
msgstr ""

#: include/meta-boxes/course_meta.php:389
msgid "Select Instructor"
msgstr ""

#: include/meta-boxes/course_meta.php:397
msgid "Instructors"
msgstr ""

#: include/meta-boxes/course_meta.php:408
msgid "Select Campus"
msgstr ""

#: include/meta-boxes/course_meta.php:417 include/post-types/campus.php:97
#: include/post-types/campus.php:98
msgid "Campus"
msgstr ""

#: include/meta-boxes/course_meta.php:429
msgid "Price Option"
msgstr ""

#: include/meta-boxes/course_meta.php:437
#: include/meta-boxes/course_meta.php:477
#: include/meta-boxes/course_meta.php:505
#: include/meta-boxes/course_meta.php:555
#: include/meta-boxes/course_meta.php:582
#: include/meta-boxes/course_meta.php:618
#: include/meta-boxes/course_meta.php:658
#: include/meta-boxes/course_meta.php:745 include/meta-boxes/event_meta.php:401
#: include/meta-boxes/event_meta.php:445 include/meta-boxes/event_meta.php:507
#: include/meta-boxes/event_meta.php:593 include/meta-boxes/event_meta.php:630
#: include/meta-boxes/event_meta.php:700 include/meta-boxes/team_meta.php:246
#: include/meta-boxes/team_meta.php:357 include/meta-boxes/team_meta.php:394
#: include/meta-boxes/team_meta.php:464 include/meta-boxes/team_meta.php:534
#: include/meta-boxes/team_meta.php:571 include/meta-boxes/team_meta.php:641
#: include/meta-boxes/team_meta.php:712 include/meta-boxes/team_meta.php:753
#: include/meta-boxes/team_meta.php:851
msgid "Title"
msgstr ""

#: include/meta-boxes/course_meta.php:440
msgid "Apply Now "
msgstr ""

#: include/meta-boxes/course_meta.php:446 include/meta-boxes/event_meta.php:410
msgid "Url"
msgstr ""

#: include/meta-boxes/course_meta.php:455
msgid "Course Price"
msgstr ""

#: include/meta-boxes/course_meta.php:468
msgid "Tabs Shortcode"
msgstr ""

#: include/meta-boxes/course_meta.php:487
msgid "Add Tab Shortcode"
msgstr ""

#: include/meta-boxes/course_meta.php:497
msgid "Team Slider Title"
msgstr ""

#: include/meta-boxes/course_meta.php:523
msgid "Team"
msgstr ""

#: include/meta-boxes/course_meta.php:535
#: include/meta-boxes/course_meta.php:544
#: include/meta-boxes/course_meta.php:565
msgid "Attachments"
msgstr ""

#: include/meta-boxes/course_meta.php:573
msgid "Course Features"
msgstr ""

#: include/meta-boxes/course_meta.php:585
msgid "Course Feature"
msgstr ""

#: include/meta-boxes/course_meta.php:612
#: include/meta-boxes/course_meta.php:697
msgid "Add Course Feature"
msgstr ""

#: include/meta-boxes/course_meta.php:619 include/meta-boxes/event_meta.php:594
#: include/meta-boxes/team_meta.php:358 include/meta-boxes/team_meta.php:535
#: include/meta-boxes/team_meta.php:713
msgid "Actions"
msgstr ""

#: include/meta-boxes/course_meta.php:653
msgid "Course Feature Setting"
msgstr ""

#: include/meta-boxes/course_meta.php:670
#: include/meta-boxes/course_meta.php:758 include/meta-boxes/event_meta.php:641
#: include/meta-boxes/event_meta.php:713 include/meta-boxes/team_meta.php:215
#: include/meta-boxes/team_meta.php:405 include/meta-boxes/team_meta.php:477
#: include/meta-boxes/team_meta.php:786 include/meta-boxes/team_meta.php:890
msgid "Description"
msgstr ""

#: include/meta-boxes/course_meta.php:682
#: include/meta-boxes/course_meta.php:771
msgid "Select icon"
msgstr ""

#: include/meta-boxes/course_meta.php:740
msgid "Course Feature Settings"
msgstr ""

#: include/meta-boxes/course_meta.php:789
msgid "Update Course Feature"
msgstr ""

#: include/meta-boxes/course_meta.php:821
#: include/meta-boxes/course_meta.php:844
msgid "Choose Color"
msgstr ""

#: include/meta-boxes/event_meta.php:17 include/meta-boxes/event_meta.php:47
msgid "Event Options"
msgstr ""

#: include/meta-boxes/event_meta.php:278
msgid "Event Start Date"
msgstr ""

#: include/meta-boxes/event_meta.php:287
msgid "Event End Date"
msgstr ""

#: include/meta-boxes/event_meta.php:296
msgid "All Day"
msgstr ""

#: include/meta-boxes/event_meta.php:311
msgid "Event Start Time"
msgstr ""

#: include/meta-boxes/event_meta.php:320
msgid "Event End Time"
msgstr ""

#: include/meta-boxes/event_meta.php:336
msgid "Repeat"
msgstr ""

#: include/meta-boxes/event_meta.php:340 include/meta-boxes/event_meta.php:356
msgid "Repeat how many time"
msgstr ""

#: include/meta-boxes/event_meta.php:343
msgid "-- Never Repeat --"
msgstr ""

#: include/meta-boxes/event_meta.php:343
msgid "Every Day"
msgstr ""

#: include/meta-boxes/event_meta.php:343
msgid "Every Week"
msgstr ""

#: include/meta-boxes/event_meta.php:343
msgid "Every Month"
msgstr ""

#: include/meta-boxes/event_meta.php:373
msgid "Ticket Option"
msgstr ""

#: include/meta-boxes/event_meta.php:385
msgid "Status"
msgstr ""

#: include/meta-boxes/event_meta.php:419
msgid "Ticket Price"
msgstr ""

#: include/meta-boxes/event_meta.php:437
msgid "Team Shortcode"
msgstr ""

#: include/meta-boxes/event_meta.php:454
msgid "Add Team Shortcode"
msgstr ""

#: include/meta-boxes/event_meta.php:465 include/meta-boxes/event_meta.php:475
#: include/meta-boxes/event_meta.php:480
msgid "Gallery"
msgstr ""

#: include/meta-boxes/event_meta.php:488
msgid "Add Gallery Images"
msgstr ""

#: include/meta-boxes/event_meta.php:498 include/meta-boxes/event_meta.php:510
msgid "Built Process"
msgstr ""

#: include/meta-boxes/event_meta.php:541
msgid "Event Speaker"
msgstr ""

#: include/meta-boxes/event_meta.php:550
msgid "Speaker"
msgstr ""

#: include/meta-boxes/event_meta.php:561
msgid "Repeat Access"
msgstr ""

#: include/meta-boxes/event_meta.php:587 include/meta-boxes/event_meta.php:654
msgid "Add Built Process"
msgstr ""

#: include/meta-boxes/event_meta.php:626
msgid "Built Process Setting"
msgstr ""

#: include/meta-boxes/event_meta.php:695
msgid "Built Process Settings"
msgstr ""

#: include/meta-boxes/event_meta.php:731
msgid "Update Built Process"
msgstr ""

#: include/meta-boxes/form_meta_fields.php:277
msgid "Full Width"
msgstr ""

#: include/meta-boxes/form_meta_fields.php:286
msgid "Sidebar Right"
msgstr ""

#: include/meta-boxes/form_meta_fields.php:295
msgid "Sidebar Left"
msgstr ""

#: include/meta-boxes/form_meta_fields.php:625
#: include/meta-boxes/form_meta_fields.php:1000
msgid "Browse"
msgstr ""

#: include/meta-boxes/form_meta_fields.php:879
msgid "Delete image"
msgstr ""

#: include/meta-boxes/form_meta_fields.php:890
msgid "Delete"
msgstr ""

#: include/meta-boxes/team_meta.php:21 include/meta-boxes/team_meta.php:48
msgid "Team Options"
msgstr ""

#: include/meta-boxes/team_meta.php:89
msgid "Contact Us Switch"
msgstr ""

#: include/meta-boxes/team_meta.php:100
msgid "Department"
msgstr ""

#: include/meta-boxes/team_meta.php:111 include/meta-boxes/team_meta.php:764
#: include/meta-boxes/team_meta.php:864
msgid "Position"
msgstr ""

#: include/meta-boxes/team_meta.php:121
msgid "Phone"
msgstr ""

#: include/meta-boxes/team_meta.php:139 templates/teams/team-slider.php:99
msgid "Email"
msgstr ""

#: include/meta-boxes/team_meta.php:149
msgid "Facebook"
msgstr ""

#: include/meta-boxes/team_meta.php:158
msgid "Twitter"
msgstr ""

#: include/meta-boxes/team_meta.php:167
msgid "Google Plus"
msgstr ""

#: include/meta-boxes/team_meta.php:176
msgid "Linkedin"
msgstr ""

#: include/meta-boxes/team_meta.php:186
msgid "Job Specifications"
msgstr ""

#: include/meta-boxes/team_meta.php:196
msgid "Job Specifications Switch"
msgstr ""

#: include/meta-boxes/team_meta.php:206
msgid "Sub Title"
msgstr ""

#: include/meta-boxes/team_meta.php:226
msgid "List"
msgstr ""

#: include/meta-boxes/team_meta.php:230
msgid "Press enter after entering a list item"
msgstr ""

#: include/meta-boxes/team_meta.php:237
msgid "Experience and Expertise"
msgstr ""

#: include/meta-boxes/team_meta.php:256
msgid "Experience Switch"
msgstr ""

#: include/meta-boxes/team_meta.php:268 include/meta-boxes/team_meta.php:277
msgid "Skills"
msgstr ""

#: include/meta-boxes/team_meta.php:287
msgid "Skills Switch"
msgstr ""

#: include/meta-boxes/team_meta.php:302 include/meta-boxes/team_meta.php:311
msgid "Testimonials"
msgstr ""

#: include/meta-boxes/team_meta.php:321
msgid "Testimonials Switch"
msgstr ""

#: include/meta-boxes/team_meta.php:351 include/meta-boxes/team_meta.php:418
msgid "Add Experience"
msgstr ""

#: include/meta-boxes/team_meta.php:390
msgid "Experiences Setting"
msgstr ""

#: include/meta-boxes/team_meta.php:459
msgid "Experiences Settings"
msgstr ""

#: include/meta-boxes/team_meta.php:495
msgid "Update Experience"
msgstr ""

#: include/meta-boxes/team_meta.php:528 include/meta-boxes/team_meta.php:595
msgid "Add Skill"
msgstr ""

#: include/meta-boxes/team_meta.php:567
msgid "Skills Setting"
msgstr ""

#: include/meta-boxes/team_meta.php:582 include/meta-boxes/team_meta.php:654
msgid "Percentage"
msgstr ""

#: include/meta-boxes/team_meta.php:636
msgid "Skills Settings"
msgstr ""

#: include/meta-boxes/team_meta.php:672
msgid "Update Skill"
msgstr ""

#: include/meta-boxes/team_meta.php:706 include/meta-boxes/team_meta.php:799
msgid "Add Testimonial"
msgstr ""

#: include/meta-boxes/team_meta.php:749
msgid "Testimonials Setting"
msgstr ""

#: include/meta-boxes/team_meta.php:775 include/meta-boxes/team_meta.php:877
msgid "Image"
msgstr ""

#: include/meta-boxes/team_meta.php:846
msgid "Testimonials Settings"
msgstr ""

#: include/meta-boxes/team_meta.php:908
msgid "Update Testimonial"
msgstr ""

#: include/post-types/campus.php:27 include/post-types/course.php:25
#: include/post-types/events.php:25
msgid "Start Date"
msgstr ""

#: include/post-types/campus.php:28 include/post-types/course.php:26
#: include/post-types/events.php:26
msgid "End Date"
msgstr ""

#: include/post-types/campus.php:29 include/post-types/course.php:27
#: include/post-types/events.php:27
msgid "Timing"
msgstr ""

#: include/post-types/campus.php:78 include/post-types/course.php:83
#: include/post-types/events.php:76
msgid "All day"
msgstr ""

#: include/post-types/campus.php:99 include/post-types/campus.php:102
msgid "Add New Campus"
msgstr ""

#: include/post-types/campus.php:100
msgid "Edit Campus"
msgstr ""

#: include/post-types/campus.php:101
msgid "New Campus Item"
msgstr ""

#: include/post-types/campus.php:103
msgid "View Campus Item"
msgstr ""

#: include/post-types/campus.php:104
msgid "Search Campus"
msgstr ""

#: include/post-types/campus.php:105 include/post-types/course.php:110
#: include/post-types/events.php:103 include/post-types/gallery.php:91
#: include/post-types/teams.php:47
msgid "Nothing found"
msgstr ""

#: include/post-types/campus.php:106 include/post-types/course.php:111
#: include/post-types/events.php:104 include/post-types/gallery.php:92
#: include/post-types/teams.php:48
msgid "Nothing found in Trash"
msgstr ""

#: include/post-types/course.php:23 include/post-types/course.php:150
#: include/post-types/events.php:23 include/post-types/events.php:141
msgid "Categories"
msgstr ""

#: include/post-types/course.php:24 include/post-types/events.php:24
msgid "Organizer"
msgstr ""

#: include/post-types/course.php:102 include/post-types/course.php:103
msgid "Course"
msgstr ""

#: include/post-types/course.php:104 include/post-types/course.php:107
msgid "Add New Course"
msgstr ""

#: include/post-types/course.php:105
msgid "Edit Course"
msgstr ""

#: include/post-types/course.php:106
msgid "New Course Item"
msgstr ""

#: include/post-types/course.php:108
msgid "View Course Item"
msgstr ""

#: include/post-types/course.php:109
msgid "Search Course"
msgstr ""

#: include/post-types/course.php:145
msgid "Course Categories"
msgstr ""

#: include/post-types/course.php:146
msgid "Search course Categories"
msgstr ""

#: include/post-types/course.php:147
msgid "Edit course Category"
msgstr ""

#: include/post-types/course.php:148
msgid "Update course Category"
msgstr ""

#: include/post-types/course.php:149 include/post-types/events.php:140
msgid "Add New Category"
msgstr ""

#: include/post-types/events.php:95 include/post-types/events.php:96
msgid "Events"
msgstr ""

#: include/post-types/events.php:97 include/post-types/events.php:100
msgid "Add New Event"
msgstr ""

#: include/post-types/events.php:98
msgid "Edit Event"
msgstr ""

#: include/post-types/events.php:99
msgid "New Event Item"
msgstr ""

#: include/post-types/events.php:101
msgid "View Event Item"
msgstr ""

#: include/post-types/events.php:102
msgid "Search Event"
msgstr ""

#: include/post-types/events.php:136
msgid "Event Categories"
msgstr ""

#: include/post-types/events.php:137
msgid "Search Event Categories"
msgstr ""

#: include/post-types/events.php:138
msgid "Edit Event Category"
msgstr ""

#: include/post-types/events.php:139
msgid "Update Event Category"
msgstr ""

#: include/post-types/gallery.php:84
msgid "Galleries"
msgstr ""

#: include/post-types/gallery.php:85 include/post-types/gallery.php:88
msgid "Add New Gallery"
msgstr ""

#: include/post-types/gallery.php:86
msgid "Edit Gallery"
msgstr ""

#: include/post-types/gallery.php:87
msgid "New Gallery Item"
msgstr ""

#: include/post-types/gallery.php:89
msgid "View Gallery Item"
msgstr ""

#: include/post-types/gallery.php:90
msgid "Search Gallery"
msgstr ""

#: include/post-types/gallery.php:115 include/post-types/gallery.php:120
msgid "Gallery Albums"
msgstr ""

#: include/post-types/gallery.php:116
msgid "Search Gallery Albums"
msgstr ""

#: include/post-types/gallery.php:117
msgid "Edit Gallery Album"
msgstr ""

#: include/post-types/gallery.php:118
msgid "Update Gallery Album"
msgstr ""

#: include/post-types/gallery.php:119
msgid "Add New Album"
msgstr ""

#: include/post-types/gallery.php:177
msgid "Image Title"
msgstr ""

#: include/post-types/gallery.php:182
msgid "Image Description"
msgstr ""

#: include/post-types/gallery.php:187
msgid "Video Link"
msgstr ""

#: include/post-types/gallery.php:192
msgid "Read More Link"
msgstr ""

#: include/post-types/gallery.php:219
msgid "Gallery Options"
msgstr ""

#: include/post-types/gallery.php:236
msgid "Gallery is Empty. Please Select Media"
msgstr ""

#: include/post-types/gallery.php:429 include/post-types/gallery.php:431
msgid "Description view layout"
msgstr ""

#: include/post-types/gallery.php:434
msgid "Images View"
msgstr ""

#: include/post-types/gallery.php:435
msgid "Carousel View"
msgstr ""

#: include/post-types/gallery.php:437
msgid "Please select Gallery Description style layout."
msgstr ""

#: include/post-types/gallery.php:447 include/post-types/gallery.php:450
#: templates/teams/team_element.php:143
msgid "Pagination"
msgstr ""

#: include/post-types/gallery.php:453 templates/course/course_element.php:144
#: templates/course/course_element.php:183
#: templates/events/event_element.php:166
#: templates/events/event_element.php:192 templates/teams/team_element.php:106
msgid "Yes"
msgstr ""

#: include/post-types/gallery.php:454 templates/course/course_element.php:145
#: templates/course/course_element.php:184
#: templates/events/event_element.php:167
#: templates/events/event_element.php:193 templates/teams/team_element.php:107
msgid "No"
msgstr ""

#: include/post-types/gallery.php:458 include/post-types/gallery.php:461
msgid "Record per page"
msgstr ""

#: include/post-types/teams.php:39 include/post-types/teams.php:40
msgid "Teams"
msgstr ""

#: include/post-types/teams.php:41 include/post-types/teams.php:44
msgid "Add New Team"
msgstr ""

#: include/post-types/teams.php:42
msgid "Edit Team"
msgstr ""

#: include/post-types/teams.php:43
msgid "New Team Item"
msgstr ""

#: include/post-types/teams.php:45
msgid "View Team Item"
msgstr ""

#: include/post-types/teams.php:46
msgid "Search Team"
msgstr ""

#: templates/course/course-classic.php:119
#: templates/course/course-modern.php:96 templates/course/course-simple.php:96
msgid "Instructor"
msgstr ""

#: templates/course/course-classic.php:128
#: templates/course/course-modern.php:105
#: templates/course/course-simple.php:106
msgid "Duration"
msgstr ""

#: templates/course/course-classic.php:137
msgid "Starts From "
msgstr ""

#: templates/course/course-modern.php:114
#: templates/course/course-simple.php:116
msgid "Starts From"
msgstr ""

#: templates/course/course-modern.php:131
msgid "No Course found."
msgstr ""

#: templates/course/course_element.php:53
msgid "Edit Course Options"
msgstr ""

#: templates/course/course_element.php:63 templates/events/event_element.php:67
#: templates/teams/team_element.php:60
msgid "Section Title"
msgstr ""

#: templates/course/course_element.php:70
msgid "Course Design Views"
msgstr ""

#: templates/course/course_element.php:80
msgid "Simple"
msgstr ""

#: templates/course/course_element.php:85
msgid "Classic"
msgstr ""

#: templates/course/course_element.php:90 templates/teams/team_element.php:74
msgid "Modern"
msgstr ""

#: templates/course/course_element.php:96
#: templates/course/course_element.php:116
#: templates/events/event_element.php:89 templates/events/event_element.php:124
msgid ""
"Please select category to show posts. If you dont select category it will "
"display all posts"
msgstr ""

#: templates/course/course_element.php:104
#: templates/events/event_element.php:112
msgid "Choose Category"
msgstr ""

#: templates/course/course_element.php:110
#: templates/events/event_element.php:118
msgid "--Select Category--"
msgstr ""

#: templates/course/course_element.php:122
#: templates/events/event_element.php:130 templates/teams/team_element.php:85
msgid "Post Order"
msgstr ""

#: templates/course/course_element.php:128 templates/teams/team_element.php:91
msgid "Asc"
msgstr ""

#: templates/course/course_element.php:129
#: templates/events/event_element.php:137 templates/teams/team_element.php:92
msgid "DESC"
msgstr ""

#: templates/course/course_element.php:138
msgid "Display Pagination"
msgstr ""

#: templates/course/course_element.php:153
#: templates/events/event_element.php:146 templates/teams/team_element.php:116
msgid "Length of Excerpt"
msgstr ""

#: templates/course/course_element.php:160
#: templates/events/event_element.php:153
msgid ""
"Enter number of character for short description text, excerpt length will "
"work only in list view."
msgstr ""

#: templates/course/course_element.php:166
#: templates/events/event_element.php:175
#: templates/gallery-styles/gallery_element.php:149
#: templates/teams/team_element.php:130
msgid "No. of Post Per Page"
msgstr ""

#: templates/course/course_element.php:173
#: templates/events/event_element.php:182
msgid "To display all the records, leave this field blank"
msgstr ""

#: templates/course/course_element.php:179
#: templates/events/event_element.php:188
msgid "Filterable"
msgstr ""

#: templates/course/course_element.php:191
#: templates/events/event_element.php:200
#: templates/gallery-styles/gallery_element.php:157
#: templates/teams/team_element.php:155
msgid "Insert"
msgstr ""

#: templates/course/course_element.php:199
#: templates/events/event_element.php:208
#: templates/gallery-styles/gallery_element.php:168
#: templates/teams/team_element.php:163
msgid "Save"
msgstr ""

#: templates/course/course_functions.php:225
#: templates/events/event_functions.php:311
msgid "Sort by"
msgstr ""

#: templates/course/single-course.php:142
msgid "Starts from:"
msgstr ""

#: templates/course/single-course.php:173
msgid "Map it"
msgstr ""

#: templates/course/single-course.php:283
msgid "Class Description"
msgstr ""

#: templates/course/single-course.php:405 templates/teams/team-grid.php:91
#: templates/teams/team-list.php:99 templates/teams/team-listing.php:101
#: templates/teams/team-modern.php:85 templates/teams/team-simple.php:75
#: templates/teams/team-slider.php:115
msgid "No team found."
msgstr ""

#: templates/events/event-calender.php:98
msgid "No Event found."
msgstr ""

#: templates/events/event-list.php:143 templates/events/single-events.php:211
msgid "Speakers:"
msgstr ""

#: templates/events/event_element.php:60
msgid "Edit Events Options"
msgstr ""

#: templates/events/event_element.php:74
msgid "Events Design Views"
msgstr ""

#: templates/events/event_element.php:80
msgid "Events Grid"
msgstr ""

#: templates/events/event_element.php:81
msgid "Events Listing"
msgstr ""

#: templates/events/event_element.php:82
msgid "Events Slider"
msgstr ""

#: templates/events/event_element.php:83
msgid "Events Calender"
msgstr ""

#: templates/events/event_element.php:95
msgid "Events Listing Types"
msgstr ""

#: templates/events/event_element.php:101
msgid "All Events"
msgstr ""

#: templates/events/event_element.php:102
msgid "Upcoming Events"
msgstr ""

#: templates/events/event_element.php:103
msgid "Past Events"
msgstr ""

#: templates/events/event_element.php:136
msgid "ASC"
msgstr ""

#: templates/events/event_element.php:160
msgid "display Pagination"
msgstr ""

#: templates/events/event_functions.php:616
#: templates/events/single-events.php:218
msgid "Add to Calender"
msgstr ""

#: templates/events/single-events.php:255
msgid "Event Description"
msgstr ""

#: templates/gallery-styles/gallery_element.php:54
msgid "Edit Gallery Options"
msgstr ""

#: templates/gallery-styles/gallery_element.php:66
msgid "Header Title"
msgstr ""

#: templates/gallery-styles/gallery_element.php:76
msgid "Choose Gallery Layout"
msgstr ""

#: templates/gallery-styles/gallery_element.php:82
msgid "Small Gallery"
msgstr ""

#: templates/gallery-styles/gallery_element.php:83
msgid "Medium Gallery"
msgstr ""

#: templates/gallery-styles/gallery_element.php:84
msgid "Gallery Slider"
msgstr ""

#: templates/gallery-styles/gallery_element.php:90
msgid ""
"Please select Gallery Layout. If you dont select category it will display "
"all posts"
msgstr ""

#: templates/gallery-styles/gallery_element.php:98
msgid "Choose Gallery Album"
msgstr ""

#: templates/gallery-styles/gallery_element.php:106
msgid "Select Gallery Album"
msgstr ""

#: templates/gallery-styles/gallery_element.php:122
msgid "Please select Gallery."
msgstr ""

#: templates/gallery-styles/gallery_element.php:129
msgid "Gallery Pagination"
msgstr ""

#: templates/gallery-styles/gallery_element.php:135
msgid "On"
msgstr ""

#: templates/gallery-styles/gallery_element.php:136
msgid "Off"
msgstr ""

#: templates/gallery-styles/gallery_element.php:141
msgid "Please Select Discription."
msgstr ""

#: templates/teams/single-team.php:98
msgid "Email:"
msgstr ""

#: templates/teams/single-team.php:108
msgid "Phone:"
msgstr ""

#: templates/teams/single-team.php:118
msgid "Address:"
msgstr ""

#: templates/teams/team_element.php:52
msgid "Edit Team Options"
msgstr ""

#: templates/teams/team_element.php:67
msgid "Team Styles"
msgstr ""

#: templates/teams/team_element.php:73
msgid "Grid"
msgstr ""

#: templates/teams/team_element.php:75
msgid "Listing"
msgstr ""

#: templates/teams/team_element.php:76
msgid "Color Listing"
msgstr ""

#: templates/teams/team_element.php:76
msgid "slider"
msgstr ""

#: templates/teams/team_element.php:77
msgid "simple"
msgstr ""

#: templates/teams/team_element.php:100
msgid "Post Description"
msgstr ""

#: templates/teams/team_element.php:123
msgid "Enter number of character for short description text."
msgstr ""

#: templates/teams/team_element.php:137
msgid "To display all the records, leave this field blank."
msgstr ""

#: templates/teams/team_element.php:147
msgid "Show Pagination"
msgstr ""

#: templates/teams/team_element.php:148
msgid "Single Page"
msgstr ""

#. Name of the plugin
msgid "CS Framework"
msgstr ""

#. Description of the plugin
msgid "Custom Post Types Management"
msgstr ""

#. URI of the plugin
#. Author URI of the plugin
msgid "http://themeforest.net/user/Chimpstudio/"
msgstr ""

#. Author of the plugin
msgid "ChimpStudio"
msgstr ""
