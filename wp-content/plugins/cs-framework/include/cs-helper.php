<?php

/*
 * Theme functions
 * 
 */


/*
 * Quick Quote shortcode ajax callback function
 */

if ( ! function_exists('cs_quote_form_submit') ) {

    function cs_quote_form_submit() {
        define('WP_USE_THEMES', false);

        $cs_quote_error_msg = '';
        foreach ( $_REQUEST as $keys => $values ) {
            $$keys = $values;
        }

        $bloginfo = get_bloginfo();
        $subjecteEmail = "(" . $bloginfo . ") Quote Form Received";
        $message = '
            <table width="100%" border="1">
              <tr>
                <td width="100"><strong>' . __('Name:', 'uoc') . '</strong></td>
                <td>' . $quote_name . '</td>
              </tr>
              <tr>
                <td><strong>' . __('Email:', 'uoc') . '</strong></td>
                <td>' . $quote_mail . '</td>
              </tr>
              <tr>
                <td><strong>' . __('Mobile Number:', 'uoc') . '</strong></td>
                <td>' . $quote_number . '</td>
              </tr>
              <tr>
                <td><strong>' . __('No of Workers:', 'uoc') . '</strong></td>
                <td>' . $quote_workers . '</td>
              </tr>
			  <tr>
                <td><strong>' . __('Date:', 'uoc') . '</strong></td>
                <td>' . $quote_date . '</td>
              </tr>
			  <tr>
                <td><strong>' . __('Time:', 'uoc') . '</strong></td>
                <td>' . $quote_time . '</td>
              </tr>
			  <tr>
                <td><strong>' . __('Address:', 'uoc') . '</strong></td>
                <td>' . $quote_address . '</td>
              </tr>
			  <tr>
                <td><strong>' . __('Message:', 'uoc') . '</strong></td>
                <td>' . $quote_message . '</td>
              </tr>
              <tr>
                <td><strong>' . __('IP Address:', 'uoc') . '</strong></td>
                <td>' . $_SERVER["REMOTE_ADDR"] . '</td>
              </tr>
            </table>';

        $headers = "From: " . $quote_name . "\r\n";
        $headers .= "Reply-To: " . $quote_mail . "\r\n";
        $headers .= "Content-type: text/html; charset=utf-8" . "\r\n";
        $headers .= "MIME-Version: 1.0" . "\r\n";
        $attachments = '';

        if ( wp_mail(sanitize_email($cs_quote_email), sanitize_email($subjecteEmail), $message, $headers, $attachments) ) {
            $json = array();
            $json['type'] = "success";
            $json['message'] = '<p>' . cs_textarea_filter($cs_quote_succ_msg) . '</p>';
        } else {
            $json['type'] = "error";
            $json['message'] = '<p>' . cs_textarea_filter($cs_quote_error_msg) . '</p>';
        };

        echo json_encode($json);
        die();
    }

}
add_action('wp_ajax_nopriv_cs_quote_form_submit', 'cs_quote_form_submit');
add_action('wp_ajax_cs_quote_form_submit', 'cs_quote_form_submit');

/*
 * Contact Form shortcode ajax callback function
 */

if ( ! function_exists('cs_contact_form_submit') ) {

    // Contact form submit ajax
    function cs_contact_form_submit() {
        define('WP_USE_THEMES', false);
        $subject = '';
        $cs_contact_error_msg = '';
        $subject_name = 'Subject';
        foreach ( $_REQUEST as $keys => $values ) {
            $$keys = $values;
        }

        if ( isset($phone) && $phone <> '' ) {
            $subject_name = 'Phone';
            $subject = $phone;
        }

        $bloginfo = get_bloginfo();
        $subjecteEmail = "(" . $bloginfo . ") Contact Form Received";
        $message = '
            <table width="100%" border="1">
              <tr>
                <td width="100"><strong>' . __('Name:', 'uoc') . '</strong></td>
                <td>' . $contact_name . '</td>
              </tr>
              <tr>
                <td><strong>' . __('Email:', 'uoc') . '</strong></td>
                <td>' . $contact_email . '</td>
              </tr>
              <tr>
                <td><strong>' . $subject_name . ':</strong></td>
                <td>' . $subject . '</td>
              </tr>
              <tr>
                <td><strong>' . __('Message:', 'uoc') . '</strong></td>
                <td>' . $contact_msg . '</td>
              </tr>
              <tr>
                <td><strong>' . __('IP Address:', 'uoc') . '</strong></td>
                <td>' . $_SERVER["REMOTE_ADDR"] . '</td>
              </tr>
            </table>';

        $headers = "From: " . $contact_name . "\r\n";
        $headers .= "Reply-To: " . $contact_email . "\r\n";
        $headers .= "Content-type: text/html; charset=utf-8" . "\r\n";
        $headers .= "MIME-Version: 1.0" . "\r\n";
        $attachments = '';

        if ( mail($cs_contact_email, sanitize_email($subjecteEmail), $message, $headers, '') ) {
            $json = array();
            $json['type'] = "success";
            $json['message'] = '<p>' . cs_textarea_filter($cs_contact_succ_msg) . '</p>';
        } else {
            $json['type'] = "error";
            $json['message'] = '<p>' . cs_textarea_filter($cs_contact_error_msg) . '</p>';
        };

        echo json_encode($json);
        die();
    }

}
add_action('wp_ajax_nopriv_cs_contact_form_submit', 'cs_contact_form_submit');
add_action('wp_ajax_cs_contact_form_submit', 'cs_contact_form_submit');
