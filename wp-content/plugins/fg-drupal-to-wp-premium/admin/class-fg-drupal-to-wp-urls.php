<?php

/**
 * URLs module
 *
 * @link       https://www.fredericgilles.net/drupal-to-wordpress/
 * @since      1.4.0
 *
 * @package    FG_Drupal_to_WordPress_Premium
 * @subpackage FG_Drupal_to_WordPress_Premium/admin
 */

if ( !class_exists('FG_Drupal_to_WordPress_Urls', false) ) {

	/**
	 * URLs class
	 *
	 * @package    FG_Drupal_to_WordPress_Premium
	 * @subpackage FG_Drupal_to_WordPress_Premium/admin
	 * @author     Frédéric GILLES
	 */
	class FG_Drupal_to_WordPress_Urls {

		/**
		 * Initialize the class and set its properties.
		 *
		 * @param    object    $plugin       Admin plugin
		 */
		public function __construct( $plugin ) {
			$this->plugin = $plugin;
		}
		
		/**
		 * Reset the Drupal last imported URL ID
		 *
		 */
		public function reset_urls() {
			update_option('fgd2wp_last_drupal_url_id', 0);
		}
		
		/**
		 * Import the URLs
		 * 
		 */
		public function import_urls() {
			if ( isset($this->plugin->premium_options['skip_redirects']) && $this->plugin->premium_options['skip_redirects'] ) {
				return;
			}
			
			if ( $this->plugin->import_stopped() ) {
				return;
			}
			
			$this->plugin->log(__('Importing redirects...', $this->plugin->get_plugin_name()));
			$imported_redirect_count = 0;
			$matches = array();
			
			$imported_nodes = $this->plugin->get_imported_drupal_posts_with_post_type();
			$imported_taxonomies = $this->plugin->get_imported_drupal_taxonomies();
			
			do {
				if ( $this->plugin->import_stopped() ) {
					return;
				}
				$urls = $this->get_urls($this->plugin->chunks_size);
				$urls_count = count($urls);

				foreach ( $urls as $url ) {
					// Increment the Drupal last imported URL ID
					update_option('fgd2wp_last_drupal_url_id', $url['pid']);
					
					if ( preg_match('#^/?(.*)/(\d+)$#', $url['source'], $matches) ) {
						$drupal_object_type = $matches[1];
						$drupal_object_id = $matches[2];
						$object_id = 0;
						$object_type = '';
						switch ( $drupal_object_type ) {
							case 'node':
								if ( isset($imported_nodes[$drupal_object_id]) ) {
									$object_id = $imported_nodes[$drupal_object_id]['post_id'];
									$object_type = $imported_nodes[$drupal_object_id]['post_type'];
								}
								break;
								
							case 'taxonomy/term':
								if ( isset($imported_taxonomies[$drupal_object_id]) ) {
									$object_id = $imported_taxonomies[$drupal_object_id]['term_id'];
									$object_type = $imported_taxonomies[$drupal_object_id]['taxonomy'];
								}
								break;
						}
						if ( !empty($object_id) && !empty($object_type) ) {
							FG_Drupal_to_WordPress_Redirect::add_redirect($url['alias'], $object_id, $object_type);
							$imported_redirect_count++;
						}
					}
				}
				$this->plugin->progressbar->increment_current_count($urls_count);
				
			} while ( ($urls != null) && ($urls_count > 0) );
			
			$this->plugin->display_admin_notice(sprintf(_n('%d redirect imported', '%d redirects imported', $imported_redirect_count, $this->plugin->get_plugin_name()), $imported_redirect_count));
		}
		
		/**
		 * Get the URLs
		 * 
		 * @param int $limit Number of urls max
		 * @return array of urls
		 */
		private function get_urls($limit=1000) {
			$urls = array();
			$prefix = $this->plugin->plugin_options['prefix'];
			$last_drupal_url_id = (int)get_option('fgd2wp_last_drupal_url_id'); // to restore the import where it left
			
			if ( version_compare($this->plugin->drupal_version, '7', '<') ) {
				// Version 6
				$source_field = 'src AS source';
				$alias_field = 'dst AS alias';
			} else {
				// Version 7+
				$source_field = 'source';
				$alias_field = 'alias';
			}
			$sql = "
				SELECT u.pid, u.${source_field}, u.${alias_field}
				FROM ${prefix}url_alias u
				WHERE u.pid > '$last_drupal_url_id'
				LIMIT $limit
			";
			
			$urls = $this->plugin->drupal_query($sql);
			return $urls;
		}
		
		/**
		 * Update the number of total elements found in Drupal
		 * 
		 * @param int $count Number of total elements
		 * @return int Number of total elements
		 */
		public function get_total_elements_count($count) {
			if ( !isset($this->plugin->premium_options['skip_redirects']) || !$this->plugin->premium_options['skip_redirects'] ) {
				$count += $this->get_urls_count();
			}
			return $count;
		}
		
		/**
		 * Get the number of URLs
		 * 
		 * @return int Number of URLs
		 */
		private function get_urls_count() {
			$count = 0;
			$prefix = $this->plugin->plugin_options['prefix'];

			$sql = "
				SELECT COUNT(*) AS nb
				FROM ${prefix}url_alias
			";
			
			$result = $this->plugin->drupal_query($sql);
			if ( isset($result[0]['nb']) ) {
				$count = $result[0]['nb'];
			}
			return $count;
		}
		
	}
}
